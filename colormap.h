/* colormap.h  (c) Markus Hoffmann   */
/* color maps ....   */


/* This file is part of RAWPIX, the image manipulator
 * =====================================================
 * RAWPIX is free software and comes with NO WARRANTY - 
 * read the file COPYING for details
 */


/* constats */ 

#define Xn 0.96720
#define Yn 1.0
#define Zn 0.81427


typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} COLOR;
typedef struct {
  double r;
  double g;
  double b;
} DCOLOR;

extern const DCOLOR darkmap[];

extern const DCOLOR lightmap[];
extern const DCOLOR lightmap2[];
extern COLOR colortable[0xffff];

/* prototypes */ 

void make_colortable(const DCOLOR *);
