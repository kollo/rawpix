# Makefile for rawpix (c) Markus Hoffmann V.1.00

# This file is part of RAWPIX, the image manipulation utility for raw images 
# ===========================================================================
# RAWPIX is free software and comes with NO WARRANTY - 
# read the file COPYING for details.

# Insert the defs for your machine

SHELL=/bin/sh

LIBNO=1.00
RELEASE=1
NAME=rawpix

# Directories
prefix=/usr
exec_prefix=${prefix}
datarootdir = ${prefix}/share

BINDIR=${exec_prefix}/bin
DATADIR=${datarootdir}
MANDIR=${datarootdir}/man

# Register variables (-ffixed-reg) -Wall
REGS=

# Optimization and debugging options
OPT=-O3
# Additional header file paths
INC= 
# definitions
DEF=
# linker stuff
LINKFLAGS = 
LIBS =  -lm

CFLAGS= $(OPT) $(INC) $(DEF) $(REGS)
# Additional header file paths
INC= 

# Compiler
CC=gcc -Wall


# Cross-Compiler fuer Windows-Excecutable
WINCC=i686-w64-mingw32-gcc

# Cross-Compiler fuer ARM-Linux-Excecutable
ARMCC=arm-linux-gcc




# files which should be added to the distribution

MANSRC= rawpix.1
DOC= README.md LICENSE 
DEBDOC= README.md

HSRC= bitmap.h  file.h colormap.h
# these are objects which are compiled to binary
MAINOBJS= rawpix.o file.o bitmap.o spat-a-fnt.o colormap.o
CSRC= $(MAINOBJS:.o=.c)

BINDIST= rawpix $(DOC) $(MANSRC)
DIST=  $(HSRC) $(CSRC) Makefile $(DOC) $(MANSRC)

all: 	rawpix

rawpix: $(MAINOBJS)
	$(CC) $(OPT) $(LINKFLAGS) -o $@ $(MAINOBJS)  $(LIBS)
	
test:  o.bin.B.bmp
	xv a.jpg &
	xv o.bin.B.bmp &
a.jpg:	
	rm -f a.jpg
	raspistill --raw -vf -hf -w 120 -h 90 -ss 170000 -o a.jpg
o.bin.B.bmp: rawpix a.jpg 
	rm -f o.bin
	rm -f o.bin.?.bmp
	./rawpix -c "Hallo, das ist ein Test" -o o.bin --bg bg.bin a.jpg --channel B
#	convert -depth 16 -size 2592x1944 gray:o.bin t.png
#	convert -depth 8 -size 1296x972 gray:o.bin.A oA.png
#	convert -depth 8 -size 1296x972 gray:o.bin.B oB.png
#	convert -depth 8 -size 1296x972 gray:o.bin.C oC.png
#	convert -depth 8 -size 1296x972 gray:o.bin.D oD.png
bg.jpg: 
	rm -f bg.jpg
	raspistill --raw -vf -hf -w 120 -h 90 -ss 170000 -o bg.jpg
bg.bin: rawpix bg.jpg
	rm -f bg.bin
	rm -f bg.bin.*
	./rawpix -c "Background image" -o bg.bin bg.jpg 
#	convert -depth 8 -size 1296x972 gray:bg.bin.A tA.png
#	convert -depth 8 -size 1296x972 gray:bg.bin.B tB.png
#	convert -depth 8 -size 1296x972 gray:bg.bin.C tC.png
#	convert -depth 8 -size 1296x972 gray:bg.bin.D tD.png

install : $(NAME) $(NAME).1
	install -s -m 755 $(NAME) $(BINDIR)/
	install -m 644 $(NAME).1 $(MANDIR)/man1/
uninstall :
	rm -f $(BINDIR)/$(NAME)
	rm -f $(MANDIR)/man1/$(NAME).1

doc-pak: $(DEBDOC) Debian/copyright Debian/changelog.Debian
	mkdir -p $@
	cp $+ $@/
	gzip -n -9 $@/changelog.Debian


deb :	$(BINDIST) doc-pak
	sudo checkinstall -D --pkgname $(NAME) --pkgversion $(LIBNO) \
	--pkgrelease $(RELEASE)  \
	--maintainer kollo@users.sourceforge.net \
	--backup \
	--pkggroup multimedia \
	--pkglicense GPL --strip=yes --stripso=yes --reset-uids
	rm -f backup-*.tgz
	sudo chown 1000 $(NAME)_$(LIBNO)-$(RELEASE)_*.deb

strip: rawpix
	strip $<


clean:
	rm -f *.bin
	rm -f *.bin.*
	rm -f *.o a.out backup-*.tgz 

distclean: clean
	rm -f $(NAME) $(NAME).exe 
	rm -rf doc-pak

# Auto dependency stuff (from info make)
%.d: %.c
	$(CC) -MM -MT $(@:.d=.o) -MT $@ $(CPPFLAGS) $< -o $@
ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),distclean)
-include $(DEPSRC:.c=.d)
endif
endif
