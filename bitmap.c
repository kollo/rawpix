/* bitmap.c  (c) Markus Hoffmann   */
/* Functions for bitmap manipulation.
 */

/* This file is part of RAWPIX, the raw image tool 
 * =====================================================
 * RAWPIX is free software and comes with NO WARRANTY - 
 * read the file COPYING for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "bitmap.h"
#include "colormap.h"
#include "file.h"


extern COLOR colortable[0xffff];

extern char *annotation;

extern const unsigned char spat_a816[];
void text(unsigned char *bitmap,int bw,int bh, int x,int y,char *t,uint32_t c) {
  unsigned char a,b;
  int i,j;
  uint32_t *ptr=(uint32_t *)(bitmap);
 // printf("bw=%d, bh=%d, ptr=%p\n",bw,bh,ptr);
 // printf("TEXT: <%s>\n",t);
  while(*t) {
    a=*t++;
    for(i=0;i<16;i++) {
      b=spat_a816[a*16+i];
      for(j=0;j<8;j++) {
        if((b>>j) & 1) {
	   ptr[(bh-1-y-i)*bw+x+7-j]=c;
	} 
//	else ptr[(bh-1-y-i)*bw+x+j]=0xffeeeeff;
      }
    }
    x+=8;
  }
}

void box(unsigned char *bitmap,int bw,int bh, int x,int y,int w,int h) {
  int i,j,k;
  if(x<0 || y<0 || x+w>=bw || y+h>=bh) return;
  printf("BOX: %dx%d %d %d %d %d\n",bw,bh,x,y,w,h);
  uint32_t *ptr=(uint32_t *)bitmap;
  for(j=0,i=0;i<h;i++) ptr[(bh-1-y-i)*bw+x+j]|=0xff085555;
  j=w;
  for(i=0;i<h;i++) ptr[(bh-1-y-i)*bw+x+j]|=0xff085555;
  i=0;
  k=(bh-1-y-i)*bw+x;
  for(j=0;j<w;j++) {
//    ptr[(bh-1-y-i)*bw+x+j]|=0xff085555;
  //  printf("j=%d\n",j);
  }
  i=h-1;
  k=(bh-1-y-i)*bw+x;
  printf("k=%d\n",k);
  for(j=0;j<w;j++) {
  
//    ptr[k+j]|=0xff085555;
//    printf("j=%d\n",j);
  }
}


double calc_noise(unsigned short *data, int bw, int bh,int sx,int sy,int sw, int sh,unsigned short *rmax) {
   double sum=0;
   unsigned short max=0;
   int i,j;
   for(i=0;i<sh;i++) {
     for(j=0;j<sw;j++) {
       sum+=(double) data[bw*(sy+i)+(sx+j)];
       if(data[bw*(sy+i)+(sx+j)]>max) max=data[bw*(sy+i)+(sx+j)];
     }
   }
   if(rmax) *rmax=max;
  return(sum/sh/sw);
}


int partitioner(unsigned char *ptr,unsigned short *data, int bw, int bh,int sx,int sy,int sw, int sh,double level) {
  static int olevel;
  static int depth=0;
  if(depth==0) olevel=level;
  depth++;
  if(sw>15 && sh>15) {
    level=olevel;
    unsigned short m1,m2,m3,m4;
    double n1=calc_noise(data,bw,bh,sx,sy,sw/2,sh/2,&m1);
    double n2=calc_noise(data,bw,bh,sx+sw/2,sy,sw/2,sh/2,&m2);
    double n3=calc_noise(data,bw,bh,sx,     sy+sh/2,sw/2,sh/2,&m3);
    double n4=calc_noise(data,bw,bh,sx+sw/2,sy+sh/2,sw/2,sh/2,&m4);
    printf("%d %d %d %d\n",m1,m2,m3,m4);
    if(n1>20*level || m1/n1>20) partitioner(ptr,data,bw,bh,sx     ,sy     ,sw/2,sh/2,n1);
    if(n2>20*level || m2/n2>20) partitioner(ptr,data,bw,bh,sx+sw/2,sy     ,sw/2,sh/2,n2);
    if(n3>20*level || m3/n3>20) partitioner(ptr,data,bw,bh,sx     ,sy+sh/2,sw/2,sh/2,n3);
    if(n4>20*level || m4/n4>20) partitioner(ptr,data,bw,bh,sx+sw/2,sy+sh/2,sw/2,sh/2,n4);
  
    depth--;
    return(1);
  } else {
    char buf[80];
    box(ptr,bw,bh,sx,sy,sw-1,sh-1);
    sprintf(buf,"n: %g ",level);
 //   text(ptr,bw,bh,sx+sw/2+8*4,sy+sh/2+8,buf,0xffffff00);
    depth--;
    return(0);
  }
}


char *make_color_bitmap(unsigned short *data, int bw, int bh) {
  char *bitmap=malloc(BITMAPFILEHEADERLEN+BITMAPINFOHEADERLEN+4*bw*bh);
  make_colortable(lightmap2);

  BITMAPFILEHEADER *header=(BITMAPFILEHEADER *)bitmap;
  header->bfType=BF_TYPE;
  
  *((int *)&bitmap[10])=BITMAPFILEHEADERLEN+BITMAPINFOHEADERLEN;
  *((int *)&bitmap[2])=BITMAPFILEHEADERLEN+BITMAPINFOHEADERLEN+4*bw*bh;
  
  BITMAPINFOHEADER *bb2=(BITMAPINFOHEADER *)(bitmap+BITMAPFILEHEADERLEN);

  unsigned char *ptr=(unsigned char *)(bitmap+BITMAPFILEHEADERLEN+BITMAPINFOHEADERLEN);
  bb2->biSize=BITMAPINFOHEADERLEN;
  bb2->biWidth=bw;
  bb2->biHeight=bh;
  bb2->biPlanes=1;
  bb2->biBitCount=32;
  bb2->biCompression=BI_RGB;
  bb2->biSizeImage=0;
  bb2->biXPelsPerMeter=0;
  bb2->biYPelsPerMeter=0;
  bb2->biClrUsed=0;
  bb2->biClrImportant=0;
  int x,y;
  double a;
  double max=0;
  int maxx=-1,maxy=-1;
  int ii;
  int cid;
  for(y=0;y<bh;y++) {
    ii=4*((bh-1-y)*bw);
    for(x=0;x<bw;x++) {
      if(y<8) a=(double)x/bw;
      else {
        a=data[y*bw+x]/(double)0xffff;
        if(a>max) {
          max=a;
	  maxx=x;
	  maxy=y;
        }
      }
      cid=(int)(a*(double)0xffff) & 0xffff;
      ptr[ii++]=colortable[cid].b;
      ptr[ii++]=colortable[cid].g;
      ptr[ii++]=colortable[cid].r;
      ptr[ii++]=0xff;
    }
  }
  char buf[80];
  sprintf(buf,"max=%g at (%d,%d)",max,maxx,maxy);
  text(ptr,bw,bh,8,16,buf,0xffffff00);
  text(ptr,bw,bh,maxx-4,8,"v",0xffffff00);
  text(ptr,bw,bh,8,maxy-8,">",0xffffff00);
  
 // partitioner(ptr,data,bw,bh,0,0,bw,bh,calc_noise(data,bw,bh,0,0,bw-1,bh-1,NULL));

  if(annotation) {
    text(ptr,bw,bh,bw/2-4*strlen(annotation),16,annotation,0xffffffff);
  }

  return(bitmap);
}

void save_color_bitmap(char *filename, unsigned short *data, int bw, int bh) {
  char *bitmap=make_color_bitmap(data, bw, bh);
  printf("--> %s\n",filename);
  bsave(filename,bitmap,BITMAPFILEHEADERLEN+BITMAPINFOHEADERLEN+4*bw*bh);
  free(bitmap);
}

