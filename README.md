<img alt="RAWPIX" src="artwork/rawpix-logo.png"/>

RAWPIX
======

(c) 2019-2021 by Markus Hoffmann

Image extraction and manipulation utility for raw images taken with the
RaspBerry Pi camera.



Description
-----------




### Important Note:

    RAWPIX is free software and comes with NO WARRANTY - read the file
    COPYING for details
    
(Basically that means, free, open source, use and modify as you like, don't
incorporate it into non-free software, no warranty of any sort, don't blame me
if it doesn't work.)
    
Please read the file INSTALL for compiling instructions.

