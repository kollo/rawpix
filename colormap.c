/* colormap.c  (c) Markus Hoffmann   */
/* Generation of colormaps for intensities.
 */

/* This file is part of RAWPIX, the raw image tool 
 * =====================================================
 * RAWPIX is free software and comes with NO WARRANTY - 
 * read the file COPYING for details.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "colormap.h"


const DCOLOR darkmap[6]= {
 {0.00,0.00,0.00},
 {0.00,0.00,0.30},
 {0.00,0.30,0.10},
 {0.60,0.00,0.00},
 {0.68,0.60,0.00},
 {1.00,1.00,1.00}
};

const DCOLOR lightmap[6]= {
 {1.00,1.00,1.00},
 {0.35,0.77,0.80},
 {0.20,0.50,0.35},
 {0.40,0.18,0.00},
 {0.45,0.00,0.00},
 {0.22,0.00,0.00}
};
const DCOLOR lightmap2[6]= {
 {1.00,1.00,1.00},
 {0.66,0.85,1.00},
 {0.60,0.98,0.50},
 {0.90,0.90,0.50},
 {0.85,0.70,0.00},
 {1.00,0.00,0.00}
};

COLOR colortable[0xffff];


void make_colortable(const DCOLOR *map) {
  if(!map) map=darkmap;
  static int ismade=0;
  if(ismade) return; 
  unsigned int i;
  double r,g,b;
  double x,y,z;
  double hL,ha,hb;
  double aa;
  double hell;
  int count;
  for(i=0;i<=0xffff;i++) {
    aa=(double)i*5/0x10000;
    if(aa<=1) {
      r=(aa*(map[1].r-map[0].r)+map[0].r);
      g=(aa*(map[1].g-map[0].g)+map[0].g);
      b=(aa*(map[1].b-map[0].b)+map[0].b);
    } else if(aa<2) {
      r=((aa-1)*(map[2].r-map[1].r)+map[1].r);
      g=((aa-1)*(map[2].g-map[1].g)+map[1].g);
      b=((aa-1)*(map[2].b-map[1].b)+map[1].b);
    } else if(aa<3) {
      r=((aa-2)*(map[3].r-map[2].r)+map[2].r);
      g=((aa-2)*(map[3].g-map[2].g)+map[2].g);
      b=((aa-2)*(map[3].b-map[2].b)+map[2].b);
    } else if(aa<4) {
      r=((aa-3)*(map[4].r-map[3].r)+map[3].r);
      g=((aa-3)*(map[4].g-map[3].g)+map[3].g);
      b=((aa-3)*(map[4].b-map[3].b)+map[3].b);
    } else if(aa>=5) {
      r=map[5].r;
      g=map[5].g;
      b=map[5].b;
    } else {
      r=((aa-4)*(map[5].r-map[4].r)+map[4].r);
      g=((aa-4)*(map[5].g-map[4].g)+map[4].g);
      b=((aa-4)*(map[5].b-map[4].b)+map[4].b);
    }
    hell=0;
    count=0;
  #if 0
    while((hell<0.99 || hell>1.01) && count<10) {
  #endif
    /* Jetzt in den XYZ Farbraum wandeln */
    x=0.4124564*r+0.3575761*g+0.1804375*b;
    y=0.2126729*r+0.7151522*g+0.0721750*b;
    z=0.0193339*r+0.1191920*g+0.9503041*b;
    
    /* Jetzt in den Lab Farbraum wandeln */
    hL=116*pow(y/Yn,1./3)-16;
    ha=500*(pow(x/Xn,1./3)-pow(y/Yn,1./3));
    hb=200*(pow(y/Yn,1./3)-pow(z/Zn,1./3));
    
    /* Helligkeitsfaktor */
    hell=(double)i/0xffff;
    printf("%d %g ",i,hell);
    if(hL<0) hell=0.1;
    else hell=(100.0/hL)*hell;
    printf(" %g ",hL/100.0);
#if 0
    r=((double)r*hell);
    g=((double)g*hell);
    b=((double)b*hell);
   // printf("%g %g %g %g\n",hell,r,g,b);
    count++;
    }
    printf("%d %d\n",i,count);
#endif
    if(r>1) r=1;
    if(g>1) g=1;
    if(b>1) b=1;
    
    colortable[i].r=(unsigned char)(255.0*r);
    colortable[i].g=(unsigned char)(255.0*g);
    colortable[i].b=(unsigned char)(255.0*b);
    printf("%g %g %g %g\n",hell,r,g,b);
  }
  ismade=1;
}
