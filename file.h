/* file.h  (c) Markus Hoffmann   */
/* File input and output ....   */


/* This file is part of RAWPIX, the image manipulator
 * =====================================================
 * RAWPIX is free software and comes with NO WARRANTY - 
 * read the file COPYING for details
 */


/* prototypes */ 


int exist(const char *filename);
size_t lof(FILE *n);
double v_timer();
int bsave(const char *name, char *adr, size_t len);
