
/* bitmap.h  (c) Markus Hoffmann   */
/* Bitmap functions ....   */


/* This file is part of RAWPIX, the image manipulator
 * =====================================================
 * RAWPIX is free software and comes with NO WARRANTY - 
 * read the file COPYING for details
 */


/* prototypes */ 



void save_color_bitmap(char *filename, unsigned short *data, int bw, int bh);



typedef struct {                     /**** BMP file header structure ****/
    unsigned short bfType;           /*0 Magic number for file */
    char        bfSize;                /*2 Size of file */
    char a; char aa; char aaa;
    unsigned short bfReserved1;      /*6 Reserved */
    unsigned short bfReserved2;      /*8 ... */
    char     bfOffBits;              /*10 Offset to bitmap data */
    char b;
    char c;
    char d;
} BITMAPFILEHEADER;
#define BF_TYPE 0x4D42             /* "MB" */
#define BITMAPFILEHEADERLEN 14
typedef struct   {                   /**** BMP file info structure ****/
    unsigned int   biSize;           /*14 Size of info header */
    int            biWidth;          /*18 Width of image */
    int            biHeight;         /*22 Height of image */
    unsigned short biPlanes;         /*26 Number of color planes */
    unsigned short biBitCount;       /*28 Number of bits per pixel */
    unsigned int   biCompression;    /*30 Type of compression to use */
    unsigned int   biSizeImage;      /*34 Size of image data */
    int            biXPelsPerMeter;  /*38 X pixels per meter */
    int            biYPelsPerMeter;  /*42 Y pixels per meter */
    unsigned int   biClrUsed;        /*46 Number of colors used */
    unsigned int   biClrImportant;   /*50 Number of important colors */
} BITMAPINFOHEADER;
#define BITMAPINFOHEADERLEN sizeof(BITMAPINFOHEADER)

/*
 * Constants for the biCompression field...
 */

#ifndef BI_RGB
#  define BI_RGB       0             /* No compression - straight BGR data */
#  define BI_RLE8      1             /* 8-bit run-length compression */
#  define BI_RLE4      2             /* 4-bit run-length compression */
#  define BI_BITFIELDS 3             /* RGB bitmap with RGB masks */
#endif

#ifndef WINDOWS
typedef struct  {                    /**** Colormap entry structure ****/
    unsigned char  rgbBlue;          /* Blue value */
    unsigned char  rgbGreen;         /* Green value */
    unsigned char  rgbRed;           /* Red value */
    unsigned char  rgbReserved;      /* Reserved */
} RGBQUAD;
#endif
