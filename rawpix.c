/* rawpix.c  (c) Markus Hoffmann   */

/* This file is part of RAWPIX, the raw image tool 
 * =====================================================
 * RAWPIX is free software and comes with NO WARRANTY - 
 * read the file COPYING for details.
 */
 
 
/* Read in jpeg from Raspberry Pi camera captured using 'raspistill --raw' 
   and extract raw file with 10-bit values stored left-justified at 16 bpp
   
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#ifdef WINDOWS
#define EX_OK 0
#define EX_IOERR -2
#define EX_NOINPUT -1
#define EX_DATAERR -2
#else
#include <sysexits.h>
#endif

#include "file.h"
#include "bitmap.h"


#define LINELEN 256            // how long a string we need to hold the filename
#define RAWBLOCKSIZE 6404096
#define HEADERSIZE 32768
#define ROWSIZE 3264 // number of bytes per row of pixels, including 24 'other' bytes at end
#define IDSIZE 4    // number of bytes in raw header ID string
#define HPIXELS 2592   // number of horizontal pixels on OV5647 sensor
#define VPIXELS 1944   // number of vertical pixels on OV5647 sensor

unsigned short imgdata[HPIXELS*VPIXELS];
unsigned short bgimgdata[HPIXELS*VPIXELS];
unsigned char channeldata[HPIXELS/2*VPIXELS/2];

char program_name[]="rawpix";
char *ifilename;
char *ofilename=NULL;
char *annotation=NULL;
char *channeldef=NULL;
char *bgfilename="bg.bin";
int verbose=1;
int dobg=0;
double gain=1;


static void intro() {
  puts("*************************************************\n"
       "*    rawpix                    V.1.00           *\n"
       "*                 by Markus Hoffmann 2019 (c)   *\n"
       "*************************************************\n");
}
static void usage() {
  printf(
    "Usage: %s [-o <filename> -h -s <level>] [<filename>,...] --- process raw image\n\n"
    " --bg <image.bin>\t--- set background image\n"
    " -o <image.bin>\t\t--- save raw image \n"
    " -v\t\t\t--- be more verbose\n"
    " -q\t\t\t--- be more quiet\n"
    " --gain <gain>\t\t--- set gain factor\n"
    " --channel <channels>\t--- set channels\n"
    " -h, --help\t\t--- Usage\n"
    ,program_name);
}

static void kommandozeile(int anzahl, char *argumente[]) {
  int count,quitflag=0;

  /* Kommandozeile bearbeiten   */
  for(count=1;count<anzahl;count++) {
    if(!strcmp(argumente[count],"--help") || !strcmp(argumente[count],"-h")) {
      intro();
      usage();
      quitflag=1;
    } else if(!strcmp(argumente[count],"-o")) {
      ofilename=argumente[++count];
    } else if(!strcmp(argumente[count],"-c")) {
      annotation=argumente[++count];
    } else if(!strcmp(argumente[count],"--channel")) {
      channeldef=argumente[++count];
    } else if(!strcmp(argumente[count],"--channels")) {
      channeldef=argumente[++count];
    } else if(!strcmp(argumente[count],"--bg")) {
      bgfilename=argumente[++count];
      dobg=1;
    } else if(!strcmp(argumente[count],"--gain")) {
      gain=atof(argumente[++count]);
    } else if (!strcmp(argumente[count],"-v")) {
      verbose++;
    } else if (!strcmp(argumente[count],"-q")) {
      verbose--;
    } else {
      ifilename=argumente[count];
    }
   }
   if(quitflag) exit(EX_OK);
}


unsigned char *loadimage(char *name, unsigned int *len) {
  unsigned long offset;  // offset into file to start reading pixel data
  int l=0;
  unsigned char *adr;
  if(verbose) printf("<-- %s ",name);
  FILE *fdis=fopen(name,"rb");
  if(fdis==NULL) {
    if(verbose) printf(" file not found!\n");
    exit(EX_NOINPUT);
  }
  l=lof(fdis);
  if(l>0) {
    if(l<RAWBLOCKSIZE) {
      fprintf(stderr, "File %s too short to contain expected 6MB RAW data.\n",name);
      exit(EX_NOINPUT);
    }
    offset = (l - RAWBLOCKSIZE) ;  // location in file the raw header starts
    fseek(fdis, offset, SEEK_SET);  

    adr=malloc(RAWBLOCKSIZE);
    l=fread(adr,1,RAWBLOCKSIZE,fdis);
  } else {
    adr=NULL;
  }
  fclose(fdis);
  if(len) *len=l;
  if(verbose) printf(" (%d Bytes)\n",l);
  return(adr);
}
void loadbgimgdata(char *name) {
  if(verbose) printf("<-- %s ",name);
  FILE *fdis=fopen(name,"rb");
  if(fdis==NULL) {
    if(verbose) printf(" file not found!\n");
    exit(EX_NOINPUT);
  }
  int l=lof(fdis);
  if(l>0) {
    if(l<sizeof(short)*HPIXELS*VPIXELS) {
      fprintf(stderr, "File %s too short to contain expected RAW bg data.\n",name);
      exit(EX_NOINPUT);
    }
    l=fread(bgimgdata,1,sizeof(short)*HPIXELS*VPIXELS,fdis);
    if(verbose) printf(" (%d Bytes)\n",l);

  } else {
    bzero(bgimgdata,sizeof(short)*HPIXELS*VPIXELS);
    if(verbose) printf(" (zero bg data)\n");
  }
  fclose(fdis);
}

void save_channel(unsigned short *buffer, int idx) {
  char n[128];
  int oy,ox;
  ox=(idx&1);
  oy=(idx&2)>>1;
  /*
  for(y=0;y<VPIXELS/2;y++) {
    for(x=0;x<HPIXELS/2;x++) {
      channeldata[y*HPIXELS/2+x]=buffer[(2*y+oy)*HPIXELS+x*2+ox]>>8;
    }
  }
  sprintf(n,"%s.%c",ofilename,'A'+idx);
  printf("--> %s\n",n);
  bsave(n,(char *)channeldata,sizeof(char)*HPIXELS/2*VPIXELS/2);
  */
  sprintf(n,"%s.%c.bmp",ofilename,'A'+idx);
  unsigned short *chan=malloc(HPIXELS/2*VPIXELS/2*sizeof(unsigned short));
  for(int y=0;y<VPIXELS/2;y++) {
    for(int x=0;x<HPIXELS/2;x++) {
      chan[y*HPIXELS/2+x]=buffer[(2*y+oy)*HPIXELS+x*2+ox];
    }
  }
  
  save_color_bitmap(n,chan, HPIXELS/2, VPIXELS/2);
  free(chan);
}


int main(int anzahl, char *argumente[]) {
  int i,j,k;       /* loop variables (Hey, I learned FORTRAN early...) */
  unsigned int filelen;  // number of bytes in file
  unsigned char *buffer;
  unsigned short pixel[HPIXELS];  // array holds 16 bits per pixel
  unsigned char split;        // single byte with 4 pairs of low-order bits
  char *linep;

  if(anzahl<2) {    /* Kommandomodus */
    intro();
    exit(EX_OK);
  } 
  kommandozeile(anzahl, argumente);    /* Kommandozeile bearbeiten */

  if(exist(ifilename)) {
    buffer=loadimage(ifilename,&filelen);
    if(verbose>1) printf(" ID:  %c %c %c %c\n",buffer[0], buffer[1], buffer[2], buffer[3]);
    if(dobg) loadbgimgdata(bgfilename);
    linep=(char *)buffer+HEADERSIZE;
    if(verbose>1) printf("P2\n%d %d\n65535\n",HPIXELS,VPIXELS);  // ASCII PGM format header  (grey bitmap, 16 bits per pixel)
    for(k=0; k < VPIXELS; k++) {  // iterate over pixel rows
      j=0;  // offset into buffer
      for(i=0;i<HPIXELS;i+=4) {  // iterate over pixel columns
        pixel[i]   = linep[j++] << 8;
        pixel[i+1] = linep[j++] << 8;
        pixel[i+2] = linep[j++] << 8;
        pixel[i+3] = linep[j++] << 8;
        split =      linep[j++];    // low-order packed bits from previous 4 pixels
        pixel[i] += split & 0b11000000;  // unpack them bits, add to 16-bit values, left-justified
        pixel[i+1] += (split & 0b00110000)<<2;
        pixel[i+2] += (split & 0b00001100)<<4;
        pixel[i+3] += (split & 0b00000011)<<6;
      }

      if(dobg) {
        if(gain!=1) {
        } else {
          for (i = 0; i < HPIXELS; i++) {
            if(bgimgdata[k*HPIXELS+i]>pixel[i]) imgdata[k*HPIXELS+i]=0;
            else imgdata[k*HPIXELS+i]=pixel[i]-bgimgdata[k*HPIXELS+i]; 
          }
        }
      } else {
        if(gain!=1) {
        } else {
          for(i=0; i<HPIXELS; i++) {
            imgdata[k*HPIXELS+i]=pixel[i];
          }
        }
      }
      linep+=ROWSIZE;
    }
#if 0
    for (i = 0; i < HPIXELS; i++) {
      printf("%d ",pixel[i]>>8);
    }
    printf("\n");  // add an end-of-line char
#endif
    free(buffer); // free up that memory we allocated
    if(ofilename) {
    if(exist(ofilename)) {
      printf("ERROR: Output filename <%s> already exist!\n",ofilename);
    } else {
      if(channeldef==NULL || (channeldef && strchr(channeldef,'X'))) {
        printf("--> %s\n",ofilename);
        bsave(ofilename,(char *)imgdata,sizeof(short)*HPIXELS*VPIXELS);
      }
      if(channeldef) {
        if(strchr(channeldef,'A')) save_channel(imgdata,0);
        if(strchr(channeldef,'B')) save_channel(imgdata,1);
        if(strchr(channeldef,'C')) save_channel(imgdata,2);
        if(strchr(channeldef,'D')) save_channel(imgdata,3);
      }
    }
    } else printf("No output file name specified. done.\n");
  } else {
    printf("ERROR: %s not found !\n",ifilename);
    exit(EX_NOINPUT);
  }
  return(EX_OK);
} 
