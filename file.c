/* file.c  (c) Markus Hoffmann   */
/* Extensions for the standard file i/o operations.
 */

/* This file is part of RAWPIX, the raw image tool 
 * =====================================================
 * RAWPIX is free software and comes with NO WARRANTY - 
 * read the file COPYING for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <sys/stat.h>

#include <dirent.h>
#include <fnmatch.h>

#include <errno.h>
#include <fcntl.h>

int exist(const char *filename) {
  struct stat fstats;
  int retc=stat(filename, &fstats);
  if(retc==-1) return(0);
  return(!0);
}

/* Returns the length of the open file n */

size_t lof(FILE *n) {	
  long position=ftell(n);
  if(position==-1) {
    printf("lof error\n");
    return(0);
  }
  if(fseek(n,0,SEEK_END)==0) {
    long laenge=ftell(n);
    if(laenge<0) printf("ftell error\n");
    if(fseek(n,position,0)<0) printf("fseek error\n"); 
    return(laenge);
  } else printf("fseek error\n");
  return(0);
}
double v_timer() {
        struct timeval t;
        struct timezone tz;
	gettimeofday(&t,&tz);
	return((double)t.tv_sec+(double)t.tv_usec/1000000);
}
/* Saves an area in memory starting at adr with length len to a file
   with filename name.
   RETURNS: 0 on success and -1 on error */
#ifndef O_BINARY
#define O_BINARY 0
#endif
#ifndef S_IRGRP
#define S_IRGRP 0
#endif

int bsave(const char *name, char *adr, size_t len) { 
  int fdis=open(name,O_CREAT|O_BINARY|O_WRONLY|O_TRUNC,S_IRUSR|S_IWUSR|S_IRGRP);
  if(fdis==-1) return(-1);
  if(write(fdis,adr,len)==-1) perror("write");
  return(close(fdis));
}
